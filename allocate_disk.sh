#!/bin/bash

if [ $1 = "--size" ] && [ $2 > 0 ]
	then 

	mkdir -p filesystem
	dd if=/dev/zero of=filesystem/virtual.dsk bs=1M count=$2
	mkfs -t ext4 filesystem/virtual.dsk

else
	echo "Incorrect usage."
fi