import socket
import sys
import atexit
import os
import re

ftp_dir='mnt/'

def cleanup(sock):
	print("Closing current socket...")
	sock.close()
	print("Bye.")

def get_ipv4_addr(host, port):
    addr = socket.getaddrinfo(host, port, socket.AF_INET, 0, socket.SOL_TCP)
    if len(addr) == 0:
    	raise Exception("There is no IPv4 address configured for host: "+host)
    return addr[0][-1]

def send_data(data, host, port):
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.connect(get_ipv4_addr(host, port))

	s.send(data.encode())
	response = s.recv(1024)
	if response.decode() != 'OK':
		print('Error: send_data encountered error.')
		return False
	s.close()
	return True

def start_server(sock_addr):
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.bind(sock_addr)
	s.listen(1024)
	atexit.register(cleanup, s)
	process_requests(s)

def process_requests(s):
	while True:
		conn, addr = s.accept()
		raw_data = conn.recv(1024)
		if raw_data == 'status'.encode():
			output = os.popen('df -B M --output=avail '+ftp_dir).read()
			avail_size = re.findall(r'\d+', output)[0]
			conn.send(avail_size.encode())
		conn.close()

if __name__=='__main__':
	if len(sys.argv) != 5:
		print("Incorrect usage.")
		exit(0)
	ftp_dir=sys.argv[4]
	send_data(sys.argv[1], sys.argv[2], int(sys.argv[3]))
	sock_addr = get_ipv4_addr(sys.argv[1], 12346)
	start_server(sock_addr)

